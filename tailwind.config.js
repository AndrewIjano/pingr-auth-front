module.exports = {
  theme: {
    fontFamily: {
      sans: ['Rubik', 'ui-sans-serif', 'system-ui'],
      serif: ['ui-serif', 'Georgia'],
      mono: ['ui-monospace', 'SFMono-Regular'],
    },
    extend: {
      colors: {
        primary: '#ff424d',
        'primary-light': '#ff5c65',
        secondary: '#202c39',
        neutral: '#e2e8f0',
        error: '#cc323f',
        'error-light': '#fae9ea',
      },
      backgroundImage: (theme) => ({
        'random-image': "url('https://source.unsplash.com/1600x400')",
        'rising-pings': "url('~/assets/pingr-background.svg')",
      }),
    },
  },
}
