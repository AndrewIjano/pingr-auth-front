const EMAIL_TYPE =
  /^(([^<>()[\].,;:\s@"]+(.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+.)+[^<>()[\].,;:\s@"]{2,})$/
const ALPHANUM_TYPE = /^[0-9a-zA-Z]+$/
const DIGIT_TYPE = /^\d+$/

export default class {
  constructor(fieldName, fieldValue) {
    this.name = fieldName
    this.value = fieldValue
    this.error = ''
  }

  isRequired() {
    if (!this.value) {
      this.setError(`The ${this.name} can't be empty`)
    }
    return this
  }

  isAlphanum() {
    if (!ALPHANUM_TYPE.test(this.value)) {
      this.setError(`The ${this.name} must be alphanumeric`)
    }
    return this
  }

  isEmail() {
    if (!EMAIL_TYPE.test(this.value)) {
      this.setError(`The email is invalid`)
    }
    return this
  }

  shouldStartWithLetter() {
    if (DIGIT_TYPE.test(this.value[0])) {
      this.setError(`The ${this.name} should start with a letter`)
    }
    return this
  }

  hasLength(min, max) {
    const length = this.value.length
    if (length < min || length > max) {
      this.setError(
        `The ${this.name} must be between ${min} and ${max} characters`
      )
    }
    return this
  }

  setError(message) {
    // the validator should return the first error
    if (!this.error) {
      this.error = message
    }
  }

  validate() {
    return this.error
  }
}
