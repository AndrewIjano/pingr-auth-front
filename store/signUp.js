export default {
  state: () => ({
    email: '',
    password: '',
    username: '',
  }),
  mutations: {
    clear(state) {
      state.email = ''
      state.password = ''
      state.username = ''
    },
    updateEmail(state, email) {
      state.email = email
    },
    updatePassword(state, password) {
      state.password = password
    },
    updateUsername(state, username) {
      state.username = username
    },
  },
  actions: {
    async registerUser({ state }) {
      return await this.$axios.post('/register', {
        email: state.email,
        username: state.username,
        password: state.password,
      })
    },
  },
}
