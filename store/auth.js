import jwt from 'jsonwebtoken'

export default {
  state: () => ({
    accessToken: '',
  }),
  mutations: {
    setAccessToken(state, token) {
      state.accessToken = token
    },
  },
  getters: {
    authenticated(state) {
      return state.accessToken !== ''
    },
    userId(state) {
      if (!state.accessToken) {
        throw new Error('Access Token is empty')
      }
      const { sub } = jwt.decode(state.accessToken)
      return sub
    },
  },
}
