export default {
  state: () => ({
    email: '',
    password: '',
  }),
  mutations: {
    clear(state) {
      state.email = ''
      state.password = ''
    },
    updateEmail(state, email) {
      state.email = email
    },
    updatePassword(state, password) {
      state.password = password
    },
  },
  actions: {
    async loginUser({ state }) {
      return await this.$axios.post('/login', {
        email: state.email,
        password: state.password,
      })
    },
  },
}
