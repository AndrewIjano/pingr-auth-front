export default {
  actions: {
    getUser({ rootState }, userId) {
      return this.$axios.get(`600/users/${userId}`, {
        headers: {
          Authorization: `Bearer ${rootState.auth.accessToken}`,
        },
      })
    },
  },
}
